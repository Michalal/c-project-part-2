using System;
using System.Diagnostics.CodeAnalysis;
using Microsoft.EntityFrameworkCore;

namespace Utils
{
    [ExcludeFromCodeCoverage]
    public class WeatherForecastContext:DbContext
    {
        public object WeatherForecast;
        public DbSet<WeatherForecast> Context { get; set; }

        public WeatherForecastContext(DbContextOptions<WeatherForecastContext> options) : base(options!)
        {
        }

        public WeatherForecastContext()
        {
            throw new NotImplementedException();
        }
    }
    public class WeatherForecast
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }

        public int TemperatureC { get; set; }

        public int TemperatureF => 32 + (int) (TemperatureC / 0.5556);

        public string Summary { get; set; }
    }
}