﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Utils;

namespace App.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }
        public DbContextOptions<WeatherForecastContext> Options { get; set; }

        [HttpGet]
        public IEnumerable<WeatherForecast> Get()
        {
            using var db = new WeatherForecastContext();
            return db.Context?.ToArray() ?? Array.Empty<WeatherForecast>();
        }
        
        
        [HttpPost]
        public void Post(WeatherForecast weatherForecast)
        {
            var contextFactory = new WeatherForecastContextFactory();
            using (var db=contextFactory.CreateDbContext(new string[]{"aaa"}))
            {
                db.Context?.Add(weatherForecast);
                db.SaveChanges();
            }
        }
    }
}