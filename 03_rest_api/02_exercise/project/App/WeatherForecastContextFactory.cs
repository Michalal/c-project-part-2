using System.Diagnostics.CodeAnalysis;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Utils;

namespace App
{
    [ExcludeFromCodeCoverage]
    public class WeatherForecastContextFactory:IDesignTimeDbContextFactory<WeatherForecastContext>
    {
        public WeatherForecastContext CreateDbContext(string[] args)
        {
            var dbOptions = new DbContextOptionsBuilder<WeatherForecastContext>();
            dbOptions.UseSqlite("Data source=database.db", a => a.MigrationsAssembly("App"));
            return new WeatherForecastContext(dbOptions.Options);
        }
    }
}