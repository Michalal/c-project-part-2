using System;
using Utils;
using Xunit;

namespace Test
{
    public class UnitTest1
    {
        [Fact]
        public void WeatherForecastTest()
        {
            var weather = new WeatherForecast {Id = 1, Date = new DateTime(2021,11,22,5,21,37), TemperatureC = 21, Summary = "AAAA"};
            Assert.Equal(1,weather.Id);
            Assert.Equal(2021,weather.Date.Year);
            Assert.Equal(11, weather.Date.Month);
            Assert.Equal(22, weather.Date.Day);
            Assert.Equal(5, weather.Date.Hour);
            Assert.Equal(21, weather.Date.Minute);
            Assert.Equal(37, weather.Date.Second);
            Assert.Equal(21,weather.TemperatureC);
            Assert.Equal(69,weather.TemperatureF);
            Assert.Equal("AAAA",weather.Summary);
        }
    }
}