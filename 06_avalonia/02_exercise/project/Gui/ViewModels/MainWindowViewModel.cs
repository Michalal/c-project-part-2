﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gui.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        public string Greeting => "Welcome to Avalonia!";
    }
}
