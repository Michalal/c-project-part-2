using Microsoft.Extensions.DependencyInjection;
using Xunit;

namespace Test
{
    internal interface IFoo
    {
        int Test();
    }

    internal class Foo : IFoo
    {
        public int Test()
        {
            return 7;
        }
    }

    internal class Bar
    {
        private readonly IFoo _foo;

        public Bar(IFoo foo)
        {
            _foo = foo;
        }

        public int Test()
        {
            return _foo.Test();
        }
    }

    public class DependencyInjectionTest
    {
        [Fact]
        public void HasSomeTypesForTesting()
        {
            var foo = new Foo();
            var bar = new Bar(foo);
            Assert.Equal(7, bar.Test());
        }

        // TODO: Add tests...
        [Fact]
        public void TestAddTransientMethod()
        {
            var serviceCollection = new ServiceCollection();
            serviceCollection.AddTransient<IFoo,Foo>();
            serviceCollection.AddTransient<Bar>();
            var serviceProvider = serviceCollection.BuildServiceProvider();
            var example1 = serviceProvider.GetService<Bar>();
            var example2 = serviceProvider.GetService<Bar>();
            Assert.NotSame(example1,example2);
            Assert.NotNull(example1);
            Assert.NotNull(example2);
        }
        
        [Fact]
        public void TestAddSingletonMethod()
        {
            var serviceCollection = new ServiceCollection();
            serviceCollection.AddSingleton<IFoo,Foo>();
            serviceCollection.AddSingleton<Bar>();
            var serviceProvider = serviceCollection.BuildServiceProvider();
            var example1 = serviceProvider.GetService<Bar>();
            var example2 = serviceProvider.GetService<Bar>();
            Assert.Same(example1,example2);
            Assert.NotNull(example1);
        }
        [Fact]
        public void TestAddScopedMethod()
        {
            var serviceCollection = new ServiceCollection();
            serviceCollection.AddScoped<IFoo,Foo>();
            serviceCollection.AddScoped<Bar>();
            var serviceProvider = serviceCollection.BuildServiceProvider();
            using var scope1 = serviceProvider.CreateScope();
            using var scope2 = serviceProvider.CreateScope();
            var example1 = scope1.ServiceProvider.GetService<Bar>();
            var example2 = scope2.ServiceProvider.GetService<Bar>();
            var example3 = scope2.ServiceProvider.GetService<Bar>();
            Assert.NotSame(example1,example2);
            Assert.Same(example3,example2);
            Assert.NotNull(example1);
            Assert.NotNull(example2);
        }
    }
}