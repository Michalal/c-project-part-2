using System;
using System.IO;
using Moq;
using Utils;
using Xunit;
using Microsoft.Extensions.Logging;

namespace Test
{
    public class DemoTest
    {
        /*[Fact]
        public void Run()
        {
            var mock = new Mock<TextWriter>();
            Console.SetOut(mock.Object);

            const string name = "John";
            mock.Setup(tw => tw.WriteLine($"Hello {name}!")).Verifiable();
            
            var demo = new Demo(name);
            demo.Run();
            
            mock.Verify();
        }*/
        [Fact]
        public void Run()
        {
            var mock = new Mock<ILogger<Demo>>();
            var demo = new Demo(mock.Object);
            
            //Console.SetOut(mock.Object);

            const string name = "John";
            demo.Run(name);
            mock.Verify( x => x.Log(
            It.IsAny<LogLevel>(),
            It.IsAny<EventId>(),
            It.Is<It.IsAnyType>((v, t) => true),
            It.IsAny<Exception>(),
            It.Is<Func<It.IsAnyType, Exception?, string>>((v, t) => true)),
                Times.Once);
        }
    }
}