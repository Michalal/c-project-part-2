﻿using System.Diagnostics.CodeAnalysis;
using Microsoft.Extensions.DependencyInjection;
using Utils;

namespace App
{
    internal static class Program
    {
        [ExcludeFromCodeCoverage]
        private static void Main()
        {
            /*var demo = new Demo("John");
            demo.Run();*/
            var serviceCollection = new ServiceCollection();
            serviceCollection.AddScoped<Demo>().AddLogging();
            var serviceProvider = serviceCollection.BuildServiceProvider();
            var demo = serviceProvider.GetService<Demo>();
            demo?.Run("John");
        }
    }
}
