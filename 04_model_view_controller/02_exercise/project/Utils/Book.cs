﻿#nullable enable
using System;
using System.Diagnostics.CodeAnalysis;
using Microsoft.EntityFrameworkCore;

namespace Utils
{
    [ExcludeFromCodeCoverage]
    public class Book
    {
        public int Id { get; set; }
        public string? Title { get; set; }
        public string? Description { get; set; }
    }
    [ExcludeFromCodeCoverage]
    public class BookDbContext: DbContext
    {
        public DbSet<Book>? Context { get; set; }

        public BookDbContext(DbContextOptions<BookDbContext>? options) : base(options!)
        {
        }
        
    }
}