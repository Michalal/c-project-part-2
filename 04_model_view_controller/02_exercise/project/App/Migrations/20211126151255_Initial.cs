﻿using System.Diagnostics.CodeAnalysis;
using Microsoft.EntityFrameworkCore.Migrations;

namespace App.Migrations
{
    [ExcludeFromCodeCoverage]
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
#pragma warning disable CA1062
            migrationBuilder.CreateTable(
#pragma warning restore CA1062
                name: "Context",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Title = table.Column<string>(type: "TEXT", nullable: true),
                    Description = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Context", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
#pragma warning disable CA1062
            migrationBuilder.DropTable(
#pragma warning restore CA1062
                name: "Context");
        }
    }
}
