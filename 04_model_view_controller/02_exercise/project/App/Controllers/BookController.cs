using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Utils;

namespace App.Controllers
{
    [ExcludeFromCodeCoverage]
    public class BookController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public BookController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }
        
        public IActionResult Index()
        {
            var bookContextFactory = new BookContextFactory();
            _logger.LogInformation("Book Index");
            using (var db = bookContextFactory.CreateDbContext(new string[1]))
            {
                var listBooks = db.Context!.Where(a => a.Id >= 1).ToList();
                ViewBag.TotalBooks = listBooks;
                return View();
            }
        }

        public IActionResult Create()
        {
            return View();
        }
        
        public IActionResult Store()
        {
            var bookContextFactory = new BookContextFactory();
            using(var db = bookContextFactory.CreateDbContext(new string[1])){
                var book1 = new Book{Title="Book1",Description = "AAAAA"};
                var book2 = new Book{Title="Book2",Description = "BBBBB"};
                db.Context!.Add(book1);
                db.Context.Add(book2);
                db.SaveChanges();
                db.Dispose();
            }
            return View();
        }
    }
}