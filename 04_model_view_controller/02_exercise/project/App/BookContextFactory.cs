using System.Diagnostics.CodeAnalysis;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Utils;

namespace App
{
    [ExcludeFromCodeCoverage]
    public class BookContextFactory : IDesignTimeDbContextFactory<BookDbContext>
    {
       
        public BookDbContext CreateDbContext(string[] args)
        {
            var options = new DbContextOptionsBuilder<BookDbContext>();
            options.UseSqlite("Data Source=books.db", a=> a.MigrationsAssembly("App"));
            
            return new BookDbContext(options.Options);
        }
        
    }
}