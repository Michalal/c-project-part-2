using App.Controllers;
using App.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using Xunit;

namespace Test
{
    public class UnitTest1
    {
        [Fact]
        public void TestHomeController()
        {
            var h = new Mock<ILogger<HomeController>>();
            using var controller = new HomeController(h.Object);
            Assert.NotNull(controller.Index());
            Assert.NotNull(controller.Privacy());
            Assert.IsType<ViewResult>(controller.Index());
            Assert.IsType<ViewResult>(controller.Privacy());
        }
        
        [Fact]
        public void TestHttpContext()
        {
            var h = new Mock<ILogger<HomeController>>();
            using var controller = new HomeController(h.Object);
            var moq = new Mock<HttpContext>();
            controller.ControllerContext = new ControllerContext {HttpContext = moq.Object};
            Assert.IsType<ViewResult>(controller.Error());
        }
        
        [Fact]
        public void TestErrorViewModel()
        {
            var error = new ErrorViewModel();
            Assert.False(error.ShowRequestId);
            error.RequestId = "Error1";
            Assert.True(error.ShowRequestId);
            Assert.Equal("Error1",error.RequestId);
        }

    }
}