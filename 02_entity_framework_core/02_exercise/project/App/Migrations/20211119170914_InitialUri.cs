﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace App.Migrations
{
    public partial class InitialUri : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
#pragma warning disable CA1062
            migrationBuilder.AddColumn<string>(
#pragma warning restore CA1062
                name: "Uri",
                table: "Projects",
                type: "TEXT",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
#pragma warning disable CA1062
            migrationBuilder.DropColumn(
#pragma warning restore CA1062
                name: "Uri",
                table: "Projects");
        }
    }
}
