﻿using System;
using System.Diagnostics.CodeAnalysis;
using Microsoft.EntityFrameworkCore;
using Utils;

namespace App
{
    public class ProjectContext : DbContext
    {
        public DbSet<Project>? Projects { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=project.db");
        }
    }

    [ExcludeFromCodeCoverage]
    internal static class Program
    {
        private static void Main()
        {
            // TODO: Create and use DB context...
            
            Console.WriteLine("Hello World!");
            using (var db = new ProjectContext())
            {
                var project = new Project
                    {Id = 1, Name = "test_name1", Description = "test_description2", CreationDate = DateTime.Now};
                var project2 = new Project
                    {Id = 2, Name = "test_name2", Description = "test_descriptionEEE", CreationDate = DateTime.Now};
                //db.Projects!.Add(project);
                db.Projects!.Add(project2);
                db.SaveChanges();
            }

            using (var db = new ProjectContext()) 
            { 
                var projects = db.Projects; 
                foreach (var project in projects!)
                { 
                    Console.WriteLine(project.Id + " " + project.Name + " " + project.Description + " " +
                                      project.CreationDate+" "+project.Uri);
                }
            }
        }
    }
}