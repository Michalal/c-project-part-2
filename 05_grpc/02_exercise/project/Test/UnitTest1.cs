using System;
using System.Threading.Tasks;
using Grpc.Core;
using Microsoft.Extensions.Logging;
using Moq;
using Proto;
using Server;
using Xunit;

namespace Test
{
    public class UnitTest1
    {

        [Fact]
        public async Task TestGreeterService()
        {
            const string name = "GreeterClient";
            var mock = new Mock<ILogger<GreeterService>>();
            var greeterService = new GreeterService(mock.Object);
            var request = new HelloRequest {Name = name};
            var reply = await greeterService.SayHello(request, null!).ConfigureAwait(false);
            Assert.Equal("Hello "+name,reply.Message);
        }
        
        [Fact]
        public async Task TestNullException()
        {
            var mock = new Mock<ILogger<GreeterService>>();
            var greeterService = new GreeterService(mock.Object);
            var mockContext = new Mock<ServerCallContext>();
            await Assert.ThrowsAsync<ArgumentNullException>(()=>greeterService.SayHello(null!, mockContext.Object));
        }
        
        [Fact]
        public async Task TestNullExceptionWrapper()
        {
            var mock = new Mock<ILogger<WrapperService>>();
            var wrapperService = new WrapperService(mock.Object);
            var mockContext = new Mock<ServerCallContext>();
            await Assert.ThrowsAsync<ArgumentNullException>(()=>wrapperService.WrapText(null!, mockContext.Object));
        }
        
        [Fact]
        public void WrapTextTest()
        {
            var mock = new Mock<ILogger<WrapperService>>();
            var wrapper=new WrapperService(mock.Object);
            var mockContext = new Mock<ServerCallContext>();
            const string text =
                "await Assert.ThrowsAsync<ArgumentNullException>(()=>wrapperService.WrapText(null!, mockContext.Object)); await Assert.ThrowsAsync<ArgumentNullException>(()=>wrapperService.WrapText(null!, mockContext.Object));";
            var request = new WrapRequest {Input = text, Columns = 2};
            var result = wrapper.WrapText(request, mockContext.Object);
            Assert.IsType<Task<WrapReply>>(result);
            mock.Verify(l => l.Log(
                LogLevel.Information,
                It.IsAny<EventId>(),
                It.Is<It.IsAnyType>((o,t)=>string.Equals("WrapText",o.ToString())),
                It.IsAny<Exception>(),
                It.IsAny<Func<It.IsAnyType, Exception, string>>()));
        }
        }
}