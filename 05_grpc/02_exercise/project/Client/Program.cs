﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Net.Http;
using System.Threading.Tasks;
using Grpc.Net.Client;
using Proto;

namespace GrpcGreeterClient
{
    [ExcludeFromCodeCoverage]
    class Program
    {
        static async Task Main(string[] args)
        {
            var httpHandler = new HttpClientHandler();

            httpHandler.ServerCertificateCustomValidationCallback = 
                HttpClientHandler.DangerousAcceptAnyServerCertificateValidator;

            using var channel = GrpcChannel.ForAddress("https://localhost:5001",
                new GrpcChannelOptions { HttpHandler = httpHandler });
            var client =  new Greeter.GreeterClient(channel);
            var request = new HelloRequest { Name = "GreeterClient" };
            var reply = await client.SayHelloAsync(request);
            Console.WriteLine("Greeting: " + reply.Message);
            const string text =
                "await Assert.ThrowsAsync<ArgumentNullException>(()=>wrapperService.WrapText(null!, mockContext.Object)); await Assert.ThrowsAsync<ArgumentNullException>(()=>wrapperService.WrapText(null!, mockContext.Object));";
            var client2 =  new Wrapper.WrapperClient(channel);
            var request2 = new WrapRequest { Input = text, Columns = 120 };
            var reply2 = await client2.WrapTextAsync(request2);
            Console.WriteLine("\nInput:\n"+text);
            Console.WriteLine("\nResult:\n" + reply2.Output);
        }
    }
}
