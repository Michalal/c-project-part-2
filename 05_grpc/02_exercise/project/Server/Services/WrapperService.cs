using System;
using System.Threading.Tasks;
using Grpc.Core;
using Microsoft.Extensions.Logging;
using Proto;

namespace Server
{
    public class WrapperService : Wrapper.WrapperBase
    {
        private readonly ILogger<WrapperService> _logger;
        public WrapperService(ILogger<WrapperService> logger)
        {
            _logger = logger;
        }

        public override Task<WrapReply> WrapText(WrapRequest request, ServerCallContext context)
        {
            if (request == null) throw new ArgumentNullException(nameof(request));
            _logger.LogInformation("WrapText");
            return Task.FromResult(new WrapReply
            {
                Output = WrapTexter(request.Input,request.Columns)
            });
        }

        private string WrapTexter(string str, int providedColumns)
        {
            if (str.Length <= providedColumns) return str;
            var divisionStart = providedColumns;
            var divisionStop = providedColumns;
            var space = str.LastIndexOf(' ', providedColumns);
            if (space != -1)
            {
                divisionStart = space;
                divisionStop = divisionStop + 1;
            }

            return str.Substring(0, divisionStart) + '\n' + WrapTexter(str.Substring(divisionStop), providedColumns);
        }
    }
}
